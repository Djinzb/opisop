export const data = [
  {drink: 'Bier', euro: [1, 2], cents: [10, 20, 30, 40, 50]},
  {drink: 'Duvel', euro: [2], cents: [10, 20, 30, 40, 50]},
  {drink: 'Cola', euro: [1], cents: [10, 20, 30, 40, 50]},
]
