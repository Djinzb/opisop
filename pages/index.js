import {useState, useEffect} from "react";
import {data} from '../utils/data'
import Countdown from "react-countdown";

export default function Home() {
  const [doors, setDoors] = useState([]);
  const [timerKey, setTimerKey] = useState(0)
  const timeOutDuration = 5000;
  const prefix = '/drinks';

    const items = [
      `${prefix}/strongbowred.png`,
      `${prefix}/strongbow.png`,
      `${prefix}/despe.png`,
      `${prefix}/cornet.png`,
      `${prefix}/duvel.png`,
      `${prefix}/stella.png`,
      `${prefix}/wittewijn.png`,
      `${prefix}/rouge.png`,
      `${prefix}/chouffe.png`,
      `${prefix}/karmeliet.png`,
      `${prefix}/kriek.png`,
      `${prefix}/cola.png`,
      `${prefix}/colazero.png`,
      `${prefix}/fuzetea.png`,
      `${prefix}/rose.png`,
      `${prefix}/gerolsteinerLemon.png`,
      `${prefix}/gerolsteinerOrange.png`,
      `${prefix}/icetea.png`,
      `${prefix}/iceteaGreen.png`,
  ];

  async function spin() {
    init(false, 2, 4);
    for (const door of doors) {
      const boxes = door.querySelector(".boxes");
      const duration = parseInt(boxes.style.transitionDuration);
      boxes.style.transform = "translateY(0)";
      await new Promise((resolve) => setTimeout(resolve, duration * 1000));
    }
    setTimerKey(timerKey + 1)
    ;
  }

  function init(firstInit = true, groups = 1, duration = 5) {
    for(const door of doors) {
      if (firstInit) {
        door.dataset.spinned = "0";
      } else if (door.dataset.spinned === "1") {
        return;
      }

      const boxes = door.querySelector(".boxes");
      const boxesClone = boxes.cloneNode(false);

      const pool = ["/lasokapi.png"];
      if (!firstInit) {
        const arr = [];
        for (let n = 0; n < (groups > 0 ? groups : 1); n++) {
          arr.push(...items);
        }
        pool.push(...shuffle(arr));

        boxesClone.addEventListener(
          "transitionstart",
          function () {
            door.dataset.spinned = "1";
            this.querySelectorAll(".box").forEach((box) => {
              box.style.filter = "blur(1px)";
            });
          },
          {once: true}
        );

        boxesClone?.addEventListener(
          "transitionend",
          function () {
            this.querySelectorAll(".box").forEach((box, index) => {
              box.style.filter = "blur(0)";
              if (index > 0) this.removeChild(box);
            });
          },
          {once: true}
        );
      }

      for (let i = pool.length - 1; i >= 0; i--) {
        const box = document.createElement("img");
        box.src = pool[i];
        box.classList.add("box");
        box.style.width = door.clientWidth + "px";
        box.textContent = pool[i];
        boxesClone.appendChild(box);
      }
      boxesClone.style.transitionDuration = `${duration > 0 ? duration : 1}s`;
      boxesClone.style.transform = `translateY(-${
        door.clientHeight * (pool.length - 1)
      }px)`;
      door.replaceChild(boxesClone, boxes);
    }
  }

  function shuffle([...arr]) {
    let m = arr.length;
    while (m) {
      const i = Math.floor(Math.random() * m--);
      [arr[m], arr[i]] = [arr[i], arr[m]];
    }
    return arr;
  }
  useEffect(() => {
    const doors = document.querySelectorAll(".door");
    setDoors(doors);
    init();
  }, [])

  const renderer = ({ hours, minutes, seconds, completed }) => {
      return <span>{minutes}:{seconds}</span>;
  };

  const restartGame = async () => {
    if(timerKey === 0) {
      await spin();
    } else {
      await waitAndSpin();
    }
  }

  const waitAndSpin = async () => {
    init();
    await new Promise((resolve) => setTimeout(resolve, 2000));
    await spin();
  }

  return (
    <div>
      <div className="app">
        <div>
          <div className="doors">
              <div className="door">
                <div className="boxes" />
              </div>

              <div className="door">
                <div className="boxes" />
              </div>

              <div className="door">
                <div className="boxes" />
              </div>
          </div>
          <div className="countdown">
            <Countdown date={Date.now() + timeOutDuration}
                       intervalDelay={0}
                       precision={3}
                       onComplete={restartGame}
                       renderer={renderer}
                       key={timerKey}
            />
          </div>
        </div>
        <div className="logo">
          <img src="/lasokapi.png" />
        </div>

        <div className="spinButton" onClick={waitAndSpin}/>


      </div>
    </div>
  )
}
